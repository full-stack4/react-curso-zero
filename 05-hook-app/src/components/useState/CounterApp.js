import React from 'react';
import "./CounterApp.css";

export default function counterApp() {
    return (
        <>
            <h1> Counter {0} </h1> 
            <hr/>

            <button className="btn btn-primary">+1</button>
        </>
    )
}
