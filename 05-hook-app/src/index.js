import React from 'react';
import ReactDOM from 'react-dom';
import CounterApp from './components/useState/CounterApp';
//import { HookApp } from './HookApp';


ReactDOM.render(
    <CounterApp />,
    document.getElementById('root')
);
