import { useEffect, useState } from "react";
import { getGifs } from "../helpers/getGifs";

export const useFetchGifs = ( category ) => {

    const [state, setState] = useState({
        data:[],
        loading: true 
    });
    // Los efectos no pueden ser async 
    // Se usa el efecto para que cargue unicamente cuando cargue la categoria 
    useEffect(() =>{
            // Retorno de todos los gifs de la categoria ingresada 
           getGifs(category)
            .then( imgs => {

                setState({
                    data: imgs,
                    loading: false
                }); 
            })
    }, [category])

    return state;

}  