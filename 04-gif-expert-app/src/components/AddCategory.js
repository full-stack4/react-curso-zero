import React, {  useState } from 'react';
import "@testing-library/jest-dom"
import  PropTypes from "prop-types";


// se desestructura el props inmediatamente 
export const AddCategory = ( {setCategories} ) => {


    // Lo inicializamos en un string vacio
    // Vamos a utilizar   el arreglo que nos devuelve useState para obtener y modificar el valor de la caja de texto

    const [inputvalue, setinputvalue] = useState("");

    //Método void que  Recibe un evento 
    const handleInputChange = (e) =>{
        setinputvalue(e.target.value);

        console.log("handleInputChange Llamado");

    };

    const handleSubmit = (e) => {
        e.preventDefault();
        console.log("HandleSubmit Llamado", inputvalue);
        if(inputvalue.trim().length > 2)
        {

            // Le pasamos el nuevo valor de la entrada del control HTML  Input 
            setCategories( c =>  [inputvalue, ...c ]);
            // Reseteo el valor del input 
            setinputvalue("");

        }
       
    };


    

    // El fragment se utiliza para agrupar elementos HTML de JSX
    
    return (
        <form onSubmit={ handleSubmit }>
            <p> { inputvalue}  </p>
            <input 
                type="text"

                // Valor inicial que nos provee inputvalue
                // siempre va a ser el ultimo valor actualizado que el usuario que escribio 
                value={ inputvalue }

                // se va disparar cada vez que la caja de texto cambie 
                onChange={ handleInputChange}

            />
        </form>
    )
}


// Mis propTypes 
AddCategory.propTypes = {
    setCategories: PropTypes.func.isRequired
}
