import { shallow } from "enzyme";
import React from "react";
import "@testing-library/jest-dom";

import { GifGrid } from "../components/GifGrid";
import { useFetchGifs } from "../../src/hooks/useFetchGifs";
jest.mock("../../src/hooks/useFetchGifs");


describe('Pruebas en  <GifGrid/>', () => {

    const category = "Boruto";

    test('debe de mostrar el componente correctamente', () => {
        
        useFetchGifs.mockReturnValue({
            data: [],
            loading: true
        });
        const wrapper = shallow(<GifGrid  category={category} />);
        expect(wrapper).toMatchSnapshot();

    });

    test('Debe de mostrar items ucuando se cargan imagenes useFetch', () => {
        
        const gifs = [{
            id: "123132",
            url: "https://gato.gif",
            title: "Cualquier cosa"
        }];

        useFetchGifs.mockReturnValue({
            data: gifs,
            loading: false
        });
        const wrapper = shallow(<GifGrid  category={category} />);

        expect(wrapper).toMatchSnapshot();
        expect(wrapper.find("p").exists()).toBe(false);
        expect(wrapper.find("GifGridItem").length ).toBe(gifs.length);
    });
});