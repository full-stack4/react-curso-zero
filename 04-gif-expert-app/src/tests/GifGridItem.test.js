import React from 'react';
import { shallow } from 'enzyme';
import "@testing-library/jest-dom";
import { GifGridItem } from "../components/GifGridItem";

describe('Pruebas en <GifGridItem/>', () => {

    const title = "Naruto";
    const url   = "https://localhost";
    
    const wrapper = shallow(<GifGridItem 
        title = {title}
        url   = {url}/>);

    test('debe de mostrar <GifGridItem/> correctamente', () => {
        
        expect(wrapper).toMatchSnapshot();

    });
    
    test('debe de tener un parragfo con el title', () => {
   
        const p  = wrapper.find("p");
        expect( p.text().trim()).toBe( title );

    });

    test('debe de tener la imgan igual al url y alt de los props', () => {
        
        const img = wrapper.find("img");
        expect( img.prop("src")).toBe(url);
        expect( img.prop("alt")).toBe(title);

    })

    test('debe de tener animate_fadeIn', () => {
        
        const div = wrapper.find("div");

        const className = div.prop("className");

        expect(className.includes( "animate__fadeIn")).toBe( true);

    });
    
});