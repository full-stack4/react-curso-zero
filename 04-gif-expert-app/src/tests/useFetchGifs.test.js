import { useFetchGifs } from "../../src/hooks/useFetchGifs";
import { renderHook } from "@testing-library/react-hooks";


describe('Pruebas en el hook useFetchGifs', () => {
    
    test('Debe de retornar el estado inicial', () => {
    
       const {result} = renderHook( ()=>  useFetchGifs( "One punch "));
       const {data, loading} = result.current;


       expect(data).toEqual([]);
       expect(loading).toBe(true);
       
        // const { data, loading} = useFetchGifs( "One punch ");
       // console.log(data, loading);

    });
});