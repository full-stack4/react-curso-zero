import { shallow } from "enzyme";
import { AddCategory } from "../components/AddCategory";
import React from 'react';

describe('Pruebas en <AddCategory />', () => {
    
    const setCategories = jest.fn();
    let wrapper = shallow(<AddCategory setCategories = { setCategories }/>);

    beforeEach(()=>{
        jest.clearAllMocks();
        wrapper = shallow(<AddCategory setCategories = { setCategories }/>);
    })

    test('debe de mostrarse correctamente', () => {
        
        expect( wrapper).toMatchSnapshot();
 
    });

    test('Debe de cambiar la caja de texto', () => {
        
        const input = wrapper.find("input");
        const value = "Hola mundo";
        input.simulate("change",{target: {value}});

        const text  = wrapper.find("p").text().trim();

        expect(text).toBe(value);
    });

    test('No debe de postear la informacion de la caja con submit', () => {
        wrapper.find("form").simulate("submit", { preventDefault: ()=> {}})
        expect(setCategories).not.toHaveBeenCalled();
    });

    test('Debe de llamar al setCategories y limpiar la caja de texto', () => {
        
        //1. Simular el input change
        const input = wrapper.find("input");
        const value = "Hola";
        input.simulate("change", {target: {value}});

        //2. simular el submit 
        wrapper.find("form").simulate("submit",{preventDefault: ()=>{}});
        
        //3. se debe de haber llamado el setCtaegories
        expect(setCategories).toHaveBeenCalled();

        //4. El valor del input debe de estar en vacío


        // se limpia en el mismo metodo HandleSubmit 
        expect(wrapper.find("input").prop("value")).toBe("");
        // const empty = "";
        // input.simulate("change", {target:{empty}});

        // const text  = wrapper.find("input").text().trim();
        // expect(text).toBe(empty);

    })
    
});