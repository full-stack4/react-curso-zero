
import React from "react";
import ReactDom from "react-dom";
import CounterApp from  "./CounterApp";


const divRoot = document.querySelector("#root");

ReactDom.render(<CounterApp value ={2} />, divRoot);