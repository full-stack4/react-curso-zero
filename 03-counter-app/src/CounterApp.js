import React, { Fragment, useState } from 'react';
import  PropTypes from "prop-types";
import "./index.css"; 

const CounterApp = ({value = 10}) =>{


    // Un Hook no es mas que una funcion 
    const [Counter, setCounter] = useState( value ); 
    // el use state retorna un arreglo

    const handleAdd = () =>{
        //setCounter( Counter +1 )
        setCounter( (c)=> c+1 );
    }

    const handleSustract = () =>{
        setCounter( (c)=> c-1 );
    }
    const handleReset = () =>{
        setCounter( value );
    }


    return (
            <Fragment>
                <h1>CounterApp</h1>
                <h2>{Counter}</h2>

                {/* Pasamos como referencia mi funcion. A diferencia de 
                las funciones normales, esta se ejecutar cuando hago click y  no cuando se renderice */}
                <button onClick={handleAdd}>+1</button>
                <button onClick={handleReset}>reset</button>
                <button onClick={handleSustract}>-1</button>

            </Fragment>
        );
}

export default CounterApp;

CounterApp.propTypes = {
    value: PropTypes.number
}
