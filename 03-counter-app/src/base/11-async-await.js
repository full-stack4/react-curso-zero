
export const getImagen = async() =>{ 

    try {
      const API_KEY = "xUhrcjqnr9Qx0oVBkl6yPQZWzTgXdRAE";

      // await: espera hasta que esta promesa termines antes de ejecutar la siguiente linea 
      const resp = await fetch(`http://api.giphy.com/v1/gifs/random?api_key=${API_KEY}`);
      
      // esperamos a que se resuelva la promesa para obtener la data
      const {data}     = await resp.json();
     
      const {url}    = await data.images.original;
      return url;


    } catch (error) {
      

      return "No existe";

    }
}




