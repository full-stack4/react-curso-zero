import React, {Fragment} from 'react';
import  PropTypes from "prop-types";
import "./index.css"; 
// tenemos 2 tipos de componentes
// Basados en clases y basados en funciones (Functional Components)


// Mi primera Functional Component 

// fn + f2 = Rename 
const PrimeraApp = ( { saludo, MiNumero, subtitulo }   ) =>
{

    // concepto fundamental Props => Propiedades 
    
    return (
        <Fragment>
            <h1>{ saludo }</h1>
            {/* <p>{ MiNumero }</p> */}
            <p>{ subtitulo }</p>
        </Fragment>
     );
}

//<pre>{ JSON.stringify(persona,null,3) }</pre>
PrimeraApp.propTypes = {
    saludo: PropTypes.string.isRequired,
    MiNumero: PropTypes.number
}
PrimeraApp.defaultProps = {
    subtitulo : "Soy un subtitulo"
}

export default PrimeraApp; 