import React from 'react';
import CounterApp from "../CounterApp";
import { shallow } from 'enzyme';
import "@testing-library/jest-dom";


describe(' Pruebas <CounterApp/>', () => {

    const wrapper = shallow(<CounterApp/>);
    
    test('debe de mostrar <CounterApp/> correctamente ', () => {

        expect( wrapper ).toMatchSnapshot();
        
    });

    
    test('debe de mostrar el valor por defecto 100 ', () => {
        const value = 100;
        const wrapper = shallow(
            <CounterApp value={value}/>
        );

        const searchValueDefault = wrapper.find("h2").text().trim();
        console.log(searchValueDefault);
        expect(value.toString()).toBe(searchValueDefault);
    });

    test('debe de incrementar con el botón +1', () => {
        
         wrapper.find("button").at(0).simulate("click");
        // console.log(aumentarBtn.html()); 

        const searchValueDefault = wrapper.find("h2").text().trim();
        expect(searchValueDefault).toBe("11");

    });

    test('debe de decrementar con el botón -1', () => {
        
         wrapper.find("button").at(2).simulate("click");
         wrapper.find("button").at(2).simulate("click");
        
         const searchValueDefault = wrapper.find("h2").text().trim();
         expect(searchValueDefault).toBe("9");

   });

    test('debe colocar el valor por defecto con el boton reset', () => {

        const value = 20;
        const wrapper = shallow(<CounterApp value={value}/>);
        wrapper.find("button").at(0).simulate("click");
        wrapper.find("button").at(0).simulate("click");
        wrapper.find("button").at(1).simulate("click");


        
        const counterText = wrapper.find("h2").text().trim();
        console.log(counterText); 

        expect(counterText).toBe(value.toString()); 

    });




});
