import { getHeroeById, getHeroesByOwner } from "../../base/08-imp-exp";
import heroes from "../../data/heroes";

describe('Pruebas funciones de Héroes', () => {
    test('debe de retornar un héroe por id', () => {

        // 1- que pasa si recibe un id que no existe
        // 2- que pasa si mando un id correcto pero no existe un héroe
        // 3- que pasaria si yo mando un 10


        const id = 1;
        const heroe = getHeroeById(id);
        const heroeData = heroes.find( h => h.id === heroe.id);
        expect( heroe).toEqual(heroeData);
    });
    test('debe retornar  undefined di heroe no existe ', () => {
        
        const id = 10;
        const heroe = getHeroeById(id);
        expect(heroe).toBe( undefined );
    });

    test('debe de retornar un arreglo con los héroes de DC ', () => {
        
        const owner = "DC";
        const heroes = getHeroesByOwner(owner);
        const heroesData =  heroes.filter( h => h.owner === owner);
        expect(heroes).toEqual(heroesData);

    });
    test('debe de retornar un arreglo con los héroes de Marvel', () => {
        
        const owner = "Marvel";
        const heroes = getHeroesByOwner(owner);
        const heroesData =  heroes.filter( h => h.owner === owner);
        expect(heroes.length).toEqual(heroesData.length );

    });



});