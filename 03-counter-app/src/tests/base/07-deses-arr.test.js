import { retornaArreglo }  from "../../base/07-deses-arr";

describe('Pruebas de desestructuración', () => {
    
    test('debe de retornar un string y un numero ', () => {
        const [letras, numeros] = retornaArreglo();
        console.log( typeof letras);

        expect(letras).toEqual("ABC");
        expect(typeof letras).toBe("string");
        expect(numeros).toBe(123);
        expect(typeof numeros).toBe("number"); 

    });
});









