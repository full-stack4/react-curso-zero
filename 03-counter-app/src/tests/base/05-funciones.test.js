import "@testing-library/jest-dom";
import { getUser, getUsuarioActivo } from "../../base/05-funciones";

describe('Pruebas en 05-funciones', () => {
    test('getUser debe de retornar un objeto ', () => {

        const userTest = {
            uid: 'ABC123',
            username: 'El_Papi1502'
        }
        const user = getUser();
        console.log(user);

        // ToEqual : va evaluar que el objeto tenga las mismas propiedades 
        expect( user ).toEqual(userTest);
    });

    test('getUsuarioActivo debe retornar un objeto activo', () => {
        
        const userTest = {
            uid: 'ABC567',
            username: "Pedro"
        }

        const user = getUsuarioActivo("Pedro");
        console.log(user);
        expect( user ).toEqual(userTest);

    });
});