import React from 'react';
import PrimeraApp from "../PrimeraApp";
import { shallow } from 'enzyme';
import "@testing-library/jest-dom";

describe('Pruebas en <PrimeraApp/>', () => {

    // test('debe de mostrar el mensaje "Hola, Soy Goku"', () => {
        
    //     const saludo = "Hola, Soy Goku";
    //     const { getByText } = render(<PrimeraApp saludo={saludo}/>);
    //     expect( getByText(saludo) ).toBeInTheDocument();

 
    // });

    test('debe de mostrar <PrimerApp/> correctamente', () => {

        const saludo = "Hola, Soy Goku";
        const wrapper = shallow(<PrimeraApp saludo={saludo}/>);

        expect( wrapper ).toMatchSnapshot();
    });
    test('debe de mostrar el subitulo enviado por props', () => {
        

        const saludo = "Hola, Soy Goku";
        const subtitulo = "Soy un subtitulo";
        const wrapper = shallow(
            <PrimeraApp 
                saludo={saludo}
                subtitulo={subtitulo}
                />);

        // Busca el párrafo
        const textoParrafo = wrapper.find("p").text();
        console.log(textoParrafo);
        expect( textoParrafo ).toBe( subtitulo );

    });

});