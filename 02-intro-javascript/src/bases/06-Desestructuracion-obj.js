//const {edad, nombre, clave} = persona;
// Desestructuración 
// Asignación desestructurante


const persona =
{
    nombre : "Tony",
    edad: 45,
    clave: "Ironman"
};

// extraer propiedades del objeto 
const {nombre} = persona; 
console.log(nombre);


// Extraer y renombrar la constante 
const {nombre:nombre2} = persona; 
console.log(nombre2);

// extraer mas de una propiedad

//const {edad, nombre, clave} = persona;
//console.log( nombre);
//console.log( edad);
//console.log( clave);

const retornaEdadPersona = (persona) =>
{
    const {edad, nombre, clave} = persona;
    console.log(edad);
}

retornaEdadPersona(persona);


// Desestructuración directamente en el argumento 
const retornaNombrePersona = ({nombre}) =>
{

    console.log(nombre);
}

retornaNombrePersona(persona);


// Desestructuración directamente en el argumento con más de un parámetro
const retornaPersona = ({nombre,edad,clave}) =>
{

    console.log(edad, nombre, clave);
}

retornaPersona(persona);

// Desestructuración directamente en el argumento con valor por defecto
const retornaPersonaDefecto = ({nombre,edad,clave, rango = "Capitan"}) =>
{

    console.log(edad, nombre, clave, rango);
}

retornaPersonaDefecto(persona);


// Desestructuración directamente en el argumento con valor por defecto
const retornaPersonaObjeto = ({nombre,edad,clave, rango = "Capitan"}) =>
{

    return{
        nombreClave:  clave,
        anios: edad
    }
}

const avenger = retornaPersonaObjeto(persona);
console.log(avenger);

// Desestructuración : retornar objeto y desestructurarlo 
const useContext = ({nombre,edad,clave, rango = "Capitan"}) =>
{

    return{
        nombreClave:  clave,
        anios: edad
    }
}

const {nombreClave, anios} = useContext(persona);
console.log(nombreClave, anios);

// Desestructuración : retornar objeto complejo anidados, desestructurarlo 
const useContextComplex = ({nombre,edad,clave, rango = "Capitan"}) =>
{

    return{
        nombreClave:  clave,
        anios: edad,
        latlng:{
            lat:14,
            lng:123
        }
    }
}

//const { latlng:{ lat , lng}} = useContextComplex(persona);
const {latlng} = useContextComplex(persona);
const  {lat, lng} = latlng

console.log(lat, lng)