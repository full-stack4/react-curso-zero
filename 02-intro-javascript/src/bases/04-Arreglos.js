// Arreglos en JS

// Un arreglo es una coleccion de información que se encuentra en una misma variable

// Arreglo vacio con 100 posiciones 
const arreglo = new Array(100);
arreglo.push(1);
console.log(arreglo);



// Inicialización Correcta
const arreglo2 = [];


// No es recomendable utilizar push 
arreglo2.push(1);
arreglo2.push(2);
arreglo2.push(3);
arreglo2.push(4);

console.log(arreglo2);


// El operador spread manda individualmente cada uno de los elementos de ese arreglo 
let arreglo3 = [...arreglo2,5];
console.log(arreglo3);

// El metodo crea un nuevo array con los resultados de la funcion 
// Callback: LLamada de vuelta .Es una función que recibe como argumento otra función y la ejecuta

const arreglo4 = arreglo2.map((x)=> x*2);
console.log(arreglo4);

