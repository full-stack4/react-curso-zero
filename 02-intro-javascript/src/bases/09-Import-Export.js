//import { heroes } from "./data/heroes";

// Cuando hacemos la importacion le damos el nombre que queramos


import heroes  from "../data/heroes";


export const getheroById = (id) =>{
    return heroes.find( heroe => heroe.id === id);
}


export const getHeroByOwner = (owner) => heroes.filter( hero => hero.owner === owner );


