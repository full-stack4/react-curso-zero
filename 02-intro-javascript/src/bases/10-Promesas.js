import { getheroById } from "./bases/09-Import-Export";

// const promesa = new Promise((resolve, reject)=>{
//     setTimeout(() => {

//         const hero =  getheroById(2)
//         resolve(hero);
//     }, 2000);
// })

// // resolve le va a transferir el valor que tenga al then 
// promesa.then((hero)=>{
//     console.log("El nombre del heros es" + hero)
// })
// .catch( err => console.warn(err));


const getHeroByIdAsync = (id) =>{
    return  new Promise((resolve, reject)=>{
        setTimeout(() => {

            const hero =  getheroById(id)
            if (hero) {
                resolve(hero);
            }
            else{
                reject("no se pudo encontrar el heroe");
            }
            
        }, 2000);
    });
}


getHeroByIdAsync(0)
    .then(hero => console.log("heroe", hero))
    .catch( err => console.log(err))
