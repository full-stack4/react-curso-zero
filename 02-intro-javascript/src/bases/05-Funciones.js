// Funciones en JS


// Normal functions 
function saludar(nombre)
{
    return `Hola, ${nombre}`;
}
console.log(saludar("Goku"));


// Funciones dentro de una constante 
const saludo = function(nombre)
{   
    return `Hola, ${nombre}`;   
}

console.log(saludo("Cristopher"));


// Función de Flecha 

const saludillo = (nombre) => 
{  
   return `Hola, ${nombre}`;
};   
console.log(saludillo("Pedro"));

// Función de flecha resumida - cuerpo con 1 return

const saludirijillo = (nombre) => `Hola, ${nombre}`;
console.log(saludirijillo("Daniela"));

// Función de Flecha sin argumento
 const getUser = () => {

    return{
        uid: "asdasd",
        username: "Perro"
    }
 }

 console.log(getUser());

 // Función de Flecha Implicito 
 const getUserImplicito = () =>  ({
        uid: "asdasd",
        username: "Perro"
    });
 

 console.log(getUserImplicito());

 // Tarea


 const usuarioActivo = (nombre) =>
 ({
    uid: "121424",
    username: nombre
 });

 console.log(usuarioActivo("Bryan"));