
// Desestructurar arreglos

const personajes = ["Goku", "Vegeta", "Trunks"];
const [ ,Vegeta,] = personajes;
console.log(Vegeta);

const retornarArreglo = () => { return ["BC",123]}
const [letras,numeros] = retornarArreglo();
console.log(letras, numeros)

// Tarea

const useState = (valor)=>{
    return [valor, ()=> {console.log("Hola mundo")}]
}

const [nombre, setNombre] = useState("Goku");

console.log(nombre);
setNombre();
