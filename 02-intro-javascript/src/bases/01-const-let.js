// variables y constantes


// variables que no van a cambiar o no se van a renombrar 
const nombre = "Cristopher";
const apellido = "Angulo"; 

// Valores que cambian 
let valorDado = 5;
valorDado = 4; 

console.log( nombre, apellido, valorDado);


// var no se debe usar 
if ( true) 
{

    // Variables de scope 
    let valorDado = 6;
    const nombre = "Rene";
    console.log(valorDado);
    console.log(nombre);

}

console.log(valorDado);