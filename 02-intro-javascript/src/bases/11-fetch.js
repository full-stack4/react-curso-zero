

const API_KEY = "xUhrcjqnr9Qx0oVBkl6yPQZWzTgXdRAE";


// retornar una promesa de tipo response 
const peticion = fetch(`http://api.giphy.com/v1/gifs/random?api_key=${API_KEY}`);



// Promesas en cadena 

peticion.then( resp => resp.json())
        .then( ({data}) => {
              const {url} = data.images.original
              const img = document.createElement("img");
              img.src = url;
              document.body.append( img );
            })
        .catch(console.warn);

// retorna un promesa que resuelve cualquier cosa 