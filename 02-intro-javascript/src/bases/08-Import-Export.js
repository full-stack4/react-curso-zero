//import { heroes } from "./data/heroes";

// Cuando hacemos la importacion le damos el nombre que queramos


import {heroes , owners}  from "../data/heroes";


const getheroById = (id) =>{
    return heroes.find( heroe => heroe.id === id);
}

const getheroByIdAndName = (id,name) =>{
    return heroes.find( heroe => heroe.id === id && heroe.name === name );
}

const getheroId = (name) => heroes.find( heroe =>  heroe.name === name);

const getHeroByOwner = (owner) => heroes.filter( hero => hero.owner === owner );

// ----  CTRL-K-C 

// const hero1 = getheroById(1);
// const hero2 = getheroById(2);
// const hero3 = getheroById(3);
// const hero4 = getheroById(4);
// const hero5 = getheroById(5);

// console.log( hero1);
// console.log( hero2);
// console.log( hero3);
// console.log( hero4);
// console.log( hero5);

const {  name:hero1 } = getheroById(1);
const {  name:hero2 } = getheroById(2);
const {  name:hero3 } = getheroById(3);
const {  name:hero4 } = getheroById(4);
const {  name:hero5 } = getheroById(5);

console.log(hero1);
console.log(hero2);
console.log(hero3);
console.log(hero4);
console.log(hero5);


const  { owner:own } = getheroByIdAndName(1,"Batman");
console.log(own);

const {id:idHero} = getheroId("Wolverine");
console.log(idHero);

const result = getHeroByOwner("DC");
 
result.forEach( hero => console.log(hero.name));