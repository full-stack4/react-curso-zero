

// cada vez que creamos un objeto en javascript 

// La llaves {} => Simbolizan un Objeto 

// Prototype es el ADN de los objetos-

// Los objetos trabajan con pares de valores 
const persona =
{
    nombre: "Tony",
    apellido: "Stark",
    edad: 45,
    direccion:
    {
        ciudad: "New Yor",
        zip : "12314",
        lat: 123123,
        lng: 123123
    }

};


console.table(  persona );

// no debe hacer nunca una copia a la referencia 
const persona2 = persona; 
persona2.nombre = "Peter"; 

console.log(persona);
console.log(persona2);

// Clone del Objeto
const persona3 = {...persona};